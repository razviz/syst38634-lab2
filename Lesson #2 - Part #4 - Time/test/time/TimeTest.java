package time;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * 
 * @author Zain Razvi
 *
 */
public class TimeTest {

	@Test
	public void testGetTotalMillisecondsRegular() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:50:05");
		assertTrue("Invalid number of milliseconds", totalMilliseconds == 5);
	}
	
	
	@Test (expected=NumberFormatException.class)
	public void testGetTotalMillisecondsException() {
		//to pass
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:50:0A");
		fail("Invalid number of milliseconds");
	}
	
	
	@Test
	public void testGetTotalMillisecondsBoundaryIn(){
		//to pass
		int totalMilliseconds = Time.getTotalMilliseconds("00:00:00:999");
		assertTrue("Invalid number of milliseconds",totalMilliseconds == 999);
	}
	
	
	@Test (expected=NumberFormatException.class)
	public void testGetTotalMillisecondsBoundaryOut(){
		//to pass
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:50:1000");
		fail("Invalid number of milliseconds");
	}


	@Test
	public void testGetTotalSecondsRegular() {
		int totalSeconds = Time.getTotalSeconds("01:01:01");
		assertTrue("The time provided does not match the result", totalSeconds == 3661);
	}
	
	@Test (expected=NumberFormatException.class)
	public void testGetTotalSecondsException() {
		int totalSeconds = Time.getTotalSeconds("01:0A:0B");
		fail("The time provided is not valid");
	}
	
	@Test
	public void testGetTotalSecondsBoundary_In() {
		int totalSeconds = Time.getTotalSeconds("00:00:00");
		assertTrue("The time provided does not match the result", totalSeconds == 0);
	}
	
	@Test (expected=NumberFormatException.class)
	public void testGetTotalSecondsBoundary_Out() {
		int totalSeconds = Time.getTotalSeconds("01:01:60");
		fail("The time provided is not valid");
	}

}
